package com.example.employees.vaadin.view;

import com.example.employees.vaadin.component.EntitiesTab;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public abstract class EntitiesCrudOnTabView extends VerticalLayout {
    private Button btnNew = new Button("Добавить");
    private Button btnEdit = new Button("Редактировать");
    private Button btnDelete = new Button("Удалить");
    private TextField search = new TextField("", "Поиск");
    private HorizontalLayout btns = new HorizontalLayout(btnNew, btnEdit, btnDelete, search);
    @Getter(AccessLevel.PROTECTED)
    private Tabs tabs = new Tabs();
    @Getter(AccessLevel.PROTECTED)
    private Map<Tab, EntitiesTab> tabComponents = new HashMap<>();

    public EntitiesCrudOnTabView() {
        addButtons();
    }

    public void addTabComponents(Collection<EntitiesTab> components) {
        addTabs(components);
    }

    private void addButtons() {
        add(btns);
        search.setValueChangeMode(ValueChangeMode.EAGER);

        search.addValueChangeListener(e -> tabComponents.values().forEach(comp -> comp.setFilter(e.getValue())));
        btnNew.addClickListener(e -> tabComponents.values().forEach(EntitiesTab::onCreateEntity));
        btnEdit.addClickListener(e -> tabComponents.values().forEach(EntitiesTab::onEditEntity));
        btnDelete.addClickListener(e -> tabComponents.values().forEach(EntitiesTab::onRemoveEntity));
    }

    private void addTabs(Collection<EntitiesTab> components) {
        Div componentsData = new Div();
        componentsData.setWidthFull();
        boolean firstIteration = true;
        for (EntitiesTab component: components) {
            Tab tab = new Tab(component.getTitle());
            tabs.add(tab);
            componentsData.add(component.getComponent());
            tabComponents.put(tab, component);
            if (firstIteration) {
                component.getComponent().setVisible(true);
                firstIteration = false;
            } else {
                component.getComponent().setVisible(false);
            }
        }
        add(tabs);
        add(componentsData);

        addTabsDefaultBehaviorListener();
    }

    private void addTabsDefaultBehaviorListener() {
        tabs.addSelectedChangeListener(event -> {
            tabComponents.values().forEach(c -> {
                c.getComponent().setVisible(false);
                c.setFilter("");
            });
            search.setValue("");
            tabComponents.get(tabs.getSelectedTab()).getComponent().setVisible(true);
        });
    }
}
