package com.example.employees.vaadin.view;

import com.example.employees.vaadin.component.CompanyEntitiesGrid;
import com.example.employees.vaadin.component.EmployeeEntitiesGrid;
import com.example.employees.vaadin.component.EntitiesTab;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@Route("")
public class CompAndEmpCrudOnTabView extends EntitiesCrudOnTabView {
    private CompanyEntitiesGrid companyEntities;
    private EmployeeEntitiesGrid employeeEntities;
    private Tab employeeTab;
    private Tab companyTab;

    @Autowired
    public CompAndEmpCrudOnTabView(CompanyEntitiesGrid companyEntities, EmployeeEntitiesGrid employeeEntities) {
        this.companyEntities = companyEntities;
        this.employeeEntities = employeeEntities;
        Collection<EntitiesTab> components = List.of(companyEntities, employeeEntities);
        addTabComponents(components);
        employeeTab = getEmployeeTab();
        companyTab = getCompanyTab();
        addLogicBetweenCompAndEmpTabs();
    }

    private void addLogicBetweenCompAndEmpTabs(){
        employeeTab.setEnabled(false);
        enableEmployeeTabIfCompanySelectedListenersAdd();
        disableEmployeeTabIfCompanyUnselectedListenersAdd();
    }

    private void enableEmployeeTabIfCompanySelectedListenersAdd() {
        companyEntities.addItemClickListener(event -> {
            if(companyEntities.getSelectedItems().isEmpty()) {
                employeeTab.setEnabled(false);
            } else {
                employeeTab.setEnabled(true);
            }
            employeeEntities.setCompany(event.getItem());
        });
    }

    private void disableEmployeeTabIfCompanyUnselectedListenersAdd() {
        companyEntities.setOnRefillDataHandler(()->employeeTab.setEnabled(false));
        getTabs().addSelectedChangeListener(event -> {
            if (event.getSelectedTab().equals(companyTab)) {
                employeeTab.setEnabled(false);
            }
        });
    }

    private Tab getEmployeeTab() {
        return getTabComponents().keySet().stream()
                .filter(tab -> getTabComponents().get(tab).getComponent().getClass().equals(EmployeeEntitiesGrid.class))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Something go wrong"));
    }

    private Tab getCompanyTab() {
        return getTabComponents().keySet().stream()
                .filter(tab -> getTabComponents().get(tab).getComponent().getClass().equals(CompanyEntitiesGrid.class))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Something go wrong"));
    }
}
