package com.example.employees.vaadin.component;


import com.example.employees.dao.CompanyDao;
import com.example.employees.domain.Company;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@SpringComponent
@UIScope
public class CompanyModalEditor extends Dialog {
    private Binder<Company> binder = new Binder<>(Company.class);

    private CompanyDao companyDao;
    private Company company;

    private TextField name = new TextField("", "Название");
    private TextField inn = new TextField("", "Инн");
    private TextField address = new TextField("", "Адрес");
    private TextField phone = new TextField("", "Телефон");
    private Button btnSave = new Button("Сохранить");
    private Button btnCancel = new Button("Отменить");
    private HorizontalLayout btns = new HorizontalLayout(btnSave, btnCancel);

    @Setter
    private OnSaveCompanyHandler onSaveCompanyHandler;
    public interface OnSaveCompanyHandler {
        void onSave();
    }

    @Autowired
    public CompanyModalEditor(CompanyDao companyDao) {
        this.companyDao = companyDao;
        addButtonsAndTextField();
        /*
        binder.forField(inn).withConverter(new StringToIntegerConverter("Please enter a number"))
                .bind(Company::getInn, Company::setInn);*/
        binder.bindInstanceFields(this);
    }

    private void addButtonsAndTextField() {
        HorizontalLayout companyFieldsLayout = new HorizontalLayout(name, inn, address, phone);
        VerticalLayout layout = new VerticalLayout(companyFieldsLayout, btns);
        add(layout);
        inn.addValueChangeListener(e -> inn.setValue(e.getValue().replaceAll("[^0-9]", "")));
        btnSave.addClickListener(e -> saveCompany());
        btnCancel.addClickListener(e -> close());
    }

    private void saveCompany() {
        companyDao.updateOrInsertCompany(company);
        onSaveCompanyHandler.onSave();
        close();
    }

    public void editCompany(Company company) {
        if (company == null) {
            close();
            return;
        }

        if (company.getId() != null) {
            this.company = companyDao.getCompaniesById(List.of(company.getId())).get(0);
        } else {
            this.company = company;
        }

        binder.setBean(this.company);
        name.focus();
    }
}