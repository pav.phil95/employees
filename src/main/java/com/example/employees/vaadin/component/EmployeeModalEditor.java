package com.example.employees.vaadin.component;

import com.example.employees.dao.EmployeeDao;
import com.example.employees.domain.Company;
import com.example.employees.domain.Employee;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import com.vaadin.flow.data.converter.StringToDateConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.convert.Jsr310Converters;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SpringComponent
@UIScope
public class EmployeeModalEditor extends Dialog {
    private Binder<Employee> binder = new Binder<>(Employee.class);

    private EmployeeDao employeeDao;
    private Employee employee;

    private TextField fullName = new TextField("", "ФИО");
    private DatePicker birthday = new DatePicker( "День рождения");
    private TextField email = new TextField("", "E-mail");
    private Button btnSave = new Button("Сохранить");
    private Button btnCancel = new Button("Отменить");
    private HorizontalLayout btns = new HorizontalLayout(btnSave, btnCancel);

    @Setter
    private OnSaveEmployeeHandler onSaveEmployeeHandler;
    public interface OnSaveEmployeeHandler {
        void onSave();
    }

    @Autowired
    public EmployeeModalEditor(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
        addButtonsAndTextFields();
        binder.forField(birthday)
                .bind(emp -> convert(emp.getBirthday()), (emp, localDate)->emp.setBirthday(convert(localDate)));
        binder.bindInstanceFields(this);
    }

    private void addButtonsAndTextFields() {
        HorizontalLayout employeeFieldsLayout = new HorizontalLayout(fullName, birthday, email);
        VerticalLayout layout = new VerticalLayout(employeeFieldsLayout, btns);
        add(layout);
        btnSave.addClickListener(e -> saveEmployee());
        btnCancel.addClickListener(e -> close());
    }

    private LocalDateTime convert(LocalDate date) {
        if(date == null)
            return null;
        return date.atStartOfDay();
    }

    private LocalDate convert(LocalDateTime dateTime) {
        if(dateTime == null)
            return null;
        return dateTime.toLocalDate();
    }

    private void saveEmployee() {
        employeeDao.updateOrInsertEmployee(employee);
        onSaveEmployeeHandler.onSave();
        close();
    }

    public void editEmployee(Employee employee) {
        if (employee == null) {
            close();
            return;
        }

        if (employee.getId() != null) {
            this.employee = employeeDao.getEmployeesById(List.of(employee.getId())).get(0);
        } else {
            this.employee = employee;
        }

        binder.setBean(this.employee);
        fullName.focus();
    }
}
