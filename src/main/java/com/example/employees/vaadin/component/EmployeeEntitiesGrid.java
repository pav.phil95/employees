package com.example.employees.vaadin.component;

import com.example.employees.dao.EmployeeDao;
import com.example.employees.domain.Company;
import com.example.employees.domain.Employee;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

@SpringComponent
@UIScope
public class EmployeeEntitiesGrid extends Grid<Employee> implements EntitiesTab {
    @Getter
    private final String title = "Сотрудники компании";
    private EmployeeDao employeeDao;
    private EmployeeModalEditor editor;
    private String filter;

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
        refillData();
    }

    @Getter
    private Company company = new Company();

    public void setCompany(Company company) {
        this.company = company;
        refillData();
    }

    @Autowired
    public EmployeeEntitiesGrid(EmployeeDao employeeDao, EmployeeModalEditor editor) {
        super(Employee.class);
        this.employeeDao = employeeDao;
        this.editor = editor;
        editor.setOnSaveEmployeeHandler(this::refillData);
    }

    @Override
    public Component getComponent() {
        refillData();
        return this;
    }

    private void refillData() {
        if (filter == null || filter.isEmpty()) {
            setItems(employeeDao.getEmployeesByCompanies(List.of(company)));
        } else {
            setItems(employeeDao.findEmployeeInCompanies(List.of(company), filter));
        }
    }

    @Override
    public void onCreateEntity() {
        if (!isVisible())
            return;
        Employee emp = new Employee();
        emp.setCompanyId(company.getId());
        editor.editEmployee(emp);
        editor.open();
    }

    @Override
    public void onEditEntity() {
        if (!isVisible())
            return;
        try {
            Set<Employee> selectedEmployees = getSelectedItems();
            Employee selectedEmployee = selectedEmployees.stream().findFirst().get();
            editor.editEmployee(selectedEmployee);
            editor.open();
        } catch (NoSuchElementException ex) {
            notify("Необходимо выбрать редактируемого сотрудника");
        }
    }

    private void notify(String message) {
        Notification notify = new Notification(message, 2000);
        notify.setPosition(Notification.Position.TOP_END);
        notify.open();
    }

    @Override
    public void onRemoveEntity() {
        if (!isVisible())
            return;
        try {
            Set<Employee> selectedEmployees = getSelectedItems();
            Employee selectedEmployee = selectedEmployees.stream().findFirst().get();
            confirmToRemoveEmployee(selectedEmployee);
        } catch (NoSuchElementException ex) {
            notify("Необходимо выбрать удаляемого сотрудника");
        }
    }


    private void confirmToRemoveEmployee(Employee employeeToRemove) {
        Button yes = new Button("Да");
        Button no = new Button("Отмена");
        Span content = new Span("Вы уверены, что хотите удалить выбранного сотрудника: " +
                employeeToRemove.getFullName() +
                "?");
        Notification notification = new Notification(content, yes, no);
        yes.addClickListener(event -> {
            employeeDao.removeEmployee(employeeToRemove);
            notification.close();
            refillData();
        });
        no.addClickListener(event -> notification.close());
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
