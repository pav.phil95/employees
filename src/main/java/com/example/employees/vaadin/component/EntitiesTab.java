package com.example.employees.vaadin.component;

import com.vaadin.flow.component.Component;

public interface EntitiesTab {
    String getTitle();
    void setFilter(String filter);
    Component getComponent();
    void onCreateEntity();
    void onEditEntity();
    void onRemoveEntity();
}
