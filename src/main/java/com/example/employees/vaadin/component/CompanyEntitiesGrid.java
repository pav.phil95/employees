package com.example.employees.vaadin.component;

import com.example.employees.dao.CompanyDao;
import com.example.employees.domain.Company;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;
import java.util.Set;

@SpringComponent
@UIScope
public class CompanyEntitiesGrid extends Grid<Company> implements EntitiesTab {
    @Getter
    private final String title = "Компании";
    private CompanyDao companyDao;
    private CompanyModalEditor editor;
    private String filter;

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
        refillData();
    }

    @Setter
    private OnRefillDataHandler onRefillDataHandler;
    public interface OnRefillDataHandler {
        void onRefill();
    }

    @Autowired
    public CompanyEntitiesGrid(CompanyDao companyDao, CompanyModalEditor editor) {
        super(Company.class);
        this.companyDao = companyDao;
        this.editor = editor;
        editor.setOnSaveCompanyHandler(this::refillData);
    }

    @Override
    public Component getComponent() {
        refillData();
        return this;
    }

    private void refillData() {
        if (filter == null || filter.isEmpty()) {
            setItems(companyDao.getCompanies());
        } else {
            setItems(companyDao.findCompanies(filter));
        }
        if(onRefillDataHandler != null)
            onRefillDataHandler.onRefill();
    }

    @Override
    public void onCreateEntity() {
        if (!isVisible())
            return;
        editor.editCompany(new Company());
        editor.open();
    }

    @Override
    public void onEditEntity() {
        if (!isVisible())
            return;
        try {
            Set<Company> selectedCompanies = getSelectedItems();
            Company selectedCompany = selectedCompanies.stream().findFirst().get();
            editor.editCompany(selectedCompany);
            editor.open();
        } catch (NoSuchElementException ex) {
            notify("Необходимо выбрать редактируемую компанию");
        }
    }

    private void notify(String message) {
        Notification notify = new Notification(message, 2000);
        notify.setPosition(Notification.Position.TOP_END);
        notify.open();
    }

    @Override
    public void onRemoveEntity() {
        if (!isVisible())
            return;
        try {
            Set<Company> selectedCompanies = getSelectedItems();
            Company selectedCompany = selectedCompanies.stream().findFirst().get();
            confirmToRemoveCompany(selectedCompany);
        } catch (NoSuchElementException ex) {
            notify("Необходимо выбрать удаляемую компанию");
        }
    }


    private void confirmToRemoveCompany(Company companyToRemove) {
        Button yes = new Button("Да");
        Button no = new Button("Отмена");
        Span content = new Span("Вы уверены, что хотите удалить выбранную компанию: " + companyToRemove.getName() + "?");
        Notification notification = new Notification(content, yes, no);
        yes.addClickListener(event -> {
            companyDao.removeCompany(companyToRemove);
            notification.close();
            refillData();
        });
        no.addClickListener(event -> notification.close());
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
