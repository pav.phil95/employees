package com.example.employees.dao.impl;

import com.example.employees.dao.EmployeeDao;
import com.example.employees.domain.Company;
import com.example.employees.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    RowMapper<Employee> ROW_MAPPER = (ResultSet resultSet, int rowNum) ->
            new Employee(resultSet.getLong("id"),
                resultSet.getString("fullname"),
                convert(resultSet.getTimestamp("birthday")),
                resultSet.getString("email"),
                resultSet.getLong("company_id")
        );

    private LocalDateTime convert(Timestamp timestamp) {
        if(timestamp == null)
            return null;
        return timestamp.toLocalDateTime();
    }

    @Autowired
    public EmployeeDaoImpl(JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }
    @Override
    public List<Employee> getEmployees() {
        return namedParameterJdbcTemplate.query("select * from employees order by id", ROW_MAPPER);
    }

    @Override
    public List<Employee> getEmployeesById(List<Long> ids) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("employeeIds", ids);
        return namedParameterJdbcTemplate.query("select * from employees where id in(:employeeIds) order by id", paramMap, ROW_MAPPER);
    }

    @Override
    @Transactional
    public Employee updateOrInsertEmployee(Employee employee) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", employee.getId());
        paramMap.put("fullname", employee.getFullName());
        paramMap.put("birthday", employee.getBirthday());
        paramMap.put("email", employee.getEmail());
        paramMap.put("company", employee.getCompanyId());
        if(employee.getId() != null) {
            namedParameterJdbcTemplate.update(
                    "update employees set fullname = :fullname, birthday = :birthday, email = :email, company_id = :company where id = :id",
                    paramMap);
        } else {
            namedParameterJdbcTemplate.update(
                    "insert into employees (fullname, birthday, email, company_id) values (:fullname, :birthday, :email, :company)",
                    paramMap);
        }
        return employee;
    }

    @Override
    public void removeEmployee(Employee employee) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("employeeId", employee.getId());
        namedParameterJdbcTemplate.update("delete from employees where id = :employeeId", paramMap);
    }

    @Override
    public List<Employee> findEmployee(String query) {
        query = query.toLowerCase();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("query", "%" + query + "%");
        return namedParameterJdbcTemplate.query(
                "select * from employees where lower(fullname) like :query or lower(email) like :query",
                paramMap,
                ROW_MAPPER);
    }

    @Override
    public List<Employee> getEmployeesByCompanies(List<Company> companies) {
        List<Long> companiesIds = companies.stream()
                .map(Company::getId)
                .collect(Collectors.toList());
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("companiesIds", companiesIds);
        return namedParameterJdbcTemplate.query(
                "select * from employees where company_id in(:companiesIds) order by id",
                paramMap,
                ROW_MAPPER);
    }

    @Override
    public List<Employee> findEmployeeInCompanies(List<Company> companies, String query) {
        query = query.toLowerCase();
        List<Long> companiesIds = companies.stream()
                .map(Company::getId)
                .collect(Collectors.toList());
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("companiesIds", companiesIds);
        paramMap.put("query", "%" + query + "%");
        return namedParameterJdbcTemplate.query(
                "select * from employees where company_id in(:companiesIds) and (lower(fullname) like :query or lower(email) like :query)",
                paramMap,
                ROW_MAPPER);
    }
}
