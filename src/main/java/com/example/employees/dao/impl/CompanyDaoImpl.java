package com.example.employees.dao.impl;

import com.example.employees.dao.CompanyDao;
import com.example.employees.domain.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier("companyDao")
public class CompanyDaoImpl implements CompanyDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    RowMapper<Company> ROW_MAPPER = (ResultSet resultSet, int rowNum) ->
            new Company(resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("inn"),
                resultSet.getString("address"),
                resultSet.getString("phone")
        );

    @Autowired
    public CompanyDaoImpl(JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public List<Company> getCompanies() {
        return namedParameterJdbcTemplate.query("select * from companies order by id", ROW_MAPPER);
    }

    @Override
    public List<Company> getCompaniesById(List<Long> ids) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("companiesIds", ids);
        return namedParameterJdbcTemplate.query(
                "select * from companies where id in(:companiesIds) order by id", paramMap, ROW_MAPPER);
    }

    @Override
    @Transactional
    public Company updateOrInsertCompany(Company company) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("companyId", company.getId());
        paramMap.put("name", company.getName());
        paramMap.put("inn", company.getInn());
        paramMap.put("address", company.getAddress());
        paramMap.put("phone", company.getPhone());
        if(company.getId() != null) {
            namedParameterJdbcTemplate.update(
                    "update companies set name = :name, inn = :inn, address = :address, phone = :phone where id = :companyId",
                    paramMap);
        } else {
            namedParameterJdbcTemplate.update(
                    "insert into companies (name, inn, address, phone) values (:name, :inn, :address, :phone)",
                    paramMap);
        }
        return company;
    }
    @Override
    public void removeCompany(Company company) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("companyId", company.getId());
        namedParameterJdbcTemplate.update("delete from companies where id = :companyId", paramMap);
    }

    @Override
    public List<Company> findCompanies(String query){
        query = query.toLowerCase();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("query", "%" + query + "%");
        return namedParameterJdbcTemplate.query(
                "select * from companies where lower(name) like :query or lower(inn) like :query or lower(address) like :query or lower(phone) like :query",
                paramMap,
                ROW_MAPPER);
    }
}
