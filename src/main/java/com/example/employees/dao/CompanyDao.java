package com.example.employees.dao;

import com.example.employees.domain.Company;

import java.util.List;

public interface CompanyDao {
    List<Company> getCompanies();
    List<Company> getCompaniesById(List<Long> ids);
    Company updateOrInsertCompany(Company company);
    void removeCompany(Company company);
    List<Company> findCompanies(String query);
}