package com.example.employees.dao;

import com.example.employees.domain.Company;
import com.example.employees.domain.Employee;

import java.util.List;

public interface EmployeeDao {
    List<Employee> getEmployees();
    List<Employee> getEmployeesById(List<Long> ids);
    Employee updateOrInsertEmployee(Employee employee);
    void removeEmployee(Employee employee);
    List<Employee> findEmployee(String query);
    List<Employee> getEmployeesByCompanies(List<Company> companies);
    List<Employee> findEmployeeInCompanies(List<Company> companies, String query);
}
