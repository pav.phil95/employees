create table if not exists companies
(
	id bigserial not null
		constraint companies_pk
			primary key,
	name varchar(255) not null,
	inn varchar(100),
	address varchar(255),
	phone varchar(20)
);

alter table companies owner to test_user;

create unique index if not exists companies_id_uindex
	on companies (id);

create table if not exists employees
(
	id bigserial not null
		constraint employees_pk
			primary key,
	fullname varchar(255),
	birthday timestamp,
	email varchar(255),
	company_id integer
		constraint employees_companies_id_fk
			references companies
				on update cascade on delete cascade
);

alter table employees owner to test_user;

create unique index if not exists employees_id_uindex
	on employees (id);

create index if not exists employees_company_id_uindex
	on employees (company_id);